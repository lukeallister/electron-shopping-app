const electron = require('electron');
const url = require('url');
const path = require('path');

const {app, BrowserWindow, Menu, ipcMain} = electron;

//set env
process.env.NODE_ENV = 'production';

let mainWindow; 
let addWindow;

// listen for app to be ready 
app.on('ready', function(){
    mainWindow = new BrowserWindow({});
    //load html into window 
    mainWindow.loadURL(url.format({
        pathname: path.join(__dirname, 'mainWindow.html'),
        protocol: 'file:',
        slashes: true
    }));
    //quit 
    mainWindow.on('closed',function(){
        app.quit();
    });
    //build menu from tepmlate 
    const mainMenu = Menu.buildFromTemplate(mainMenuTemplate);
    Menu.setApplicationMenu(mainMenu);
});

//add new item window 
function createAddWindow(){
    addWindow = new BrowserWindow({
        width: 300,
        height: 200,
        title: 'Add shopping list item'
    });
    //load html into window 
    addWindow.loadURL(url.format({
        pathname: path.join(__dirname, 'addWindow.html'),
        protocol: 'file:',
        slashes: true
    }));
    //garbage
    addWindow.on('close',function(){
        addWindow = null;
    });
}

// catch item:add
ipcMain.on('item:add',function(e, item){
    mainWindow.webContents.send('item:add', item);
    addWindow.close();
});

ipcMain.on('addBtn',createAddWindow);

//menu template 
const mainMenuTemplate = [
{
    label:'File',
    submenu: [
        {
            label: 'Add Item',
            click(){
                createAddWindow();
            }
        },
        {
            label: 'Clear items',
            click(){
                mainWindow.webContents.send('item:clear');
            }
        },
        {
            label: 'Quit',
            accelerator: process.platform == 'darwin' ? 'Command+Q' : 'Ctrl+Q',
            click(){
                app.quit();
            }
        }
    ]
}
];

//add empty menu object if mac 
if(process.platform == 'darwin') {
    mainMenuTemplate.unshift({});
}

//add devtools item if not in production mode  
if(process.env.NODE_ENV !== 'production'){
    mainMenuTemplate.push({
        label:'Devtools',
        submenu:[
        {
            label:'toggle devtools',
            accelerator: process.platform == 'darwin' ? 'Command+I' : 'Ctrl+I',
            click(item, focusedWindow){
                focusedWindow.toggleDevTools();
            }
        },
        {
            role:'reload'
        }
        ]
    });
}
